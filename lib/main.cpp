#include "AVLTree.h"

int main(void) {

    AVLTree<double>* treeOne = new AVLTree<double>();
    AVLTree<double>* treeTwo = new AVLTree<double>();
    AVLTree<double>* treeThree = new AVLTree<double>();

    double randArr[50];
    int temp = 1;
    for (int x = 0; x < 50; x++) {
        randArr[x] = temp;
        temp+=2;
    }
    for (int x = 0; x < 50; x++) {
        treeOne->insert(x);
        treeTwo->insert(50-x);
        treeThree->insert(randArr[x]);
    }

    treeOne->printAllHeights(treeOne->getRoot());
    cout << "Height of tree one: " << treeOne->getHeight(treeOne->getRoot()) << " Valid? " << treeOne->validate() << endl << endl;

    treeTwo->printAllHeights(treeTwo->getRoot());
    cout << "Height of tree two: " << treeTwo->getHeight(treeTwo->getRoot()) << " Valid? " << treeOne->validate() << endl << endl;
    
    treeThree->printAllHeights(treeThree->getRoot());
    cout << "Height of tree three: " << treeThree->getHeight(treeThree->getRoot()) << " Valid? " << treeOne->validate() << endl;

    return 0;
}