#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;
using std::cin;
using std::max;
using std::min;

template <typename T>
class AVLNode {

public:

AVLNode() {
    height = 0;
    AVLLeft = nullptr;
    AVLRight = nullptr;
}
AVLNode(T newData, int newHeight) {
    data = newData;
    height = newHeight;
    AVLLeft = nullptr;
    AVLRight = nullptr;
}
~AVLNode() {
    free(&data);
    free(&height);
    free(AVLLeft);
    free(AVLRight);
}

void setData(T newData) {
    data = newData;
}
void setHeight(int newHeight) {
    height = newHeight;
}
void setLeft(AVLNode<T>* newLeft) {
    AVLLeft = newLeft;
}
void setRight(AVLNode<T>* newRight) {
    AVLRight = newRight;
}

int getHeight() {
    return height;
}
T getData() {
    return data;
}
AVLNode<T>* getLeft() {
    return AVLLeft;
}
AVLNode<T>* getRight() {
    return AVLRight;
}

private:

T data;
AVLNode<T>* AVLLeft;
AVLNode<T>* AVLRight;
int height;

};