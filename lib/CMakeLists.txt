cmake_minimum_required(VERSION 2.8.12)
project(AVLTree
        LANGUAGES CXX)
add_library(lib main.cpp AVLNode.h AVLTree.h)

add_executable(AVLTree main.cpp)

target_link_libraries(AVLTree PRIVATE lib)