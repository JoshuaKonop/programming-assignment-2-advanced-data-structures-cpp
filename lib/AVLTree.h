#include "AVLNode.h"

template <typename T>
class AVLTree {

public:
    //Constructor/Destructor
    AVLTree() {
        rootNode = nullptr;
    }
    ~AVLTree() {
        destroy(rootNode);
    }

    //Overloaded insertion function
    void insert(T value) {  //Inserts off of some integer value
        AVLNode<T>* temp = new AVLNode<T>(value, 0);

        insert(temp, rootNode, nullptr, nullptr);   //Calls other insert 
    }
    //Actual insert function
    //Parameters are child node, parent node (for traversal), grandparent (for single rotation), and great-grandparent (for double rotation)
    void insert(AVLNode<T>* node, AVLNode<T>* nodeParent, AVLNode<T>* nodeGrandParent, AVLNode<T>* nodeGreatGrandParent) {
        if (nodeParent == nullptr) {    //Base case- If rootnode is empty
            rootNode = node;
        }
        else {
            //Increase height of the node
            node->setHeight(node->getHeight() + 1);

            //If node data < parent's data
            if (node->getData() < nodeParent->getData()) {
                //Insert on the left

                //If Parent.left is empty (open to insert)
                if (nodeParent->getLeft() == nullptr) {    
                    
                    //Insert at that node
                    nodeParent->setLeft(node);
                    int key = checkAVL(rootNode, node);

                }
                //If Parent.left is not empty
                else {
                    //Go left. Set grandparent to parent and great-grandparent to grandparent
                    insert(node, nodeParent->getLeft(), nodeParent, nodeGrandParent);
                }
            }
        
            //Equivalent code on the right
            if (node->getData() > nodeParent->getData()) {

                if (nodeParent->getRight() == nullptr) {

                    nodeParent->setRight(node);
                    int key = checkAVL(rootNode, node);
                }
                else {
                    insert(node, nodeParent->getRight(), nodeParent, nodeGrandParent);
                }
            }
        }
    }

    void deleteNode(AVLNode<T>* node) {
        //If node has no children (leaf node), destroy it
        if (node->getLeft() == nullptr && node->getRight() == nullptr) {
            ~node;
        }
        //Otherwise, set node to right node and delete (not 100% working, wanted to finish assignment first- deletion not required)
        else {
            AVLNode<T>* markedNode = node;
            node = node->getRight();
            int key = checkAVL(rootNode, node);   //Need to fix here!
            rotate(node, key);

            //Then deletes the node
            ~markedNode;
        }
    }

    //Deletes the whole tree (destructor)
    void destroy(AVLNode<T>* node) {
        destroy(node->getLeft());
        destroy(node->getRight());
        delete(node);
    }

    //contains function
    bool contains(T findData, AVLNode<T>* currentNode) {
        if(findData == currentNode->getData()) {    //If the data being searched for matches our data
            return true;                            //Return true
        }
        else {  //Otherwise look left, then right
            if (findData < currentNode->getData()) {
                contains(findData, currentNode->getLeft());
            }
            if (findData > currentNode->getData()){
                contains(findData, currentNode->getRight());
            }
        }
        //Return false if node is not found
        return false;
    }

   
    //Rotate left function
    void rotateLeft(AVLNode<T>* node) {

        //Create swap (new primary node) and swapLeft (left child of swap)
        AVLNode<T>* swap = new AVLNode<T>(node->getRight()->getData(), node->getHeight());
        AVLNode<T>* swapLeft = new AVLNode<T>(node->getData(), node->getRight()->getHeight());
        
        //Create right child of swap
        AVLNode<T>* swapRight;  //depending on what node is the unbalanced, set swapRight
        if (node->getRight()->getRight() == nullptr) {
            swapRight = new AVLNode<T>(node->getRight()->getLeft()->getData(), node->getRight()->getHeight());
        }
        else {
            swapRight = new AVLNode<T>(node->getRight()->getRight()->getData(), node->getRight()->getHeight());
        }
        
        //Establish new grandchildren
        if (node->getLeft() != nullptr) {
            swapLeft->setLeft(node->getLeft());
            iterateHeight(swapLeft->getLeft(), 1);
        }
        if (node->getRight()->getLeft() != nullptr) {
            swapLeft->setRight(node->getRight()->getLeft());
        }
        if (node->getRight()->getRight()->getRight() != nullptr) {
            swapRight->setRight(node->getRight()->getRight()->getRight());
            iterateHeight(swapRight->getRight(), 0);
        }
        if (node->getRight()->getRight()->getLeft() != nullptr) {
            swapRight->setLeft(node->getRight()->getRight()->getLeft());
            iterateHeight(swapRight->getLeft(), 0);
        }
        swap->setLeft(swapLeft);    //Set swap node
        swap->setRight(swapRight);

        //Set node to swap
        *node = *swap;
    } 

    //Rotate right is equivalent to rotate left, just with left and right swapped
    void rotateRight(AVLNode<T>* node) {

        AVLNode<T>* swap = new AVLNode<T>(node->getLeft()->getData(), node->getHeight());
        AVLNode<T>* swapRight = new AVLNode<T>(node->getData(), node->getLeft()->getHeight());

        AVLNode<T>* swapLeft;
        if (node->getLeft()->getLeft() == nullptr) {
            swapLeft = new AVLNode<T>(node->getLeft()->getRight()->getData(), node->getLeft()->getHeight());
        }
        else {
            swapLeft = new AVLNode<T>(node->getLeft()->getLeft()->getData(), node->getLeft()->getHeight());
        }
        
        if (node->getRight() != nullptr) {
            swapRight->setRight(node->getRight());
            iterateHeight(swapRight->getRight(), 1);
        }
        if (node->getLeft()->getRight() != nullptr) {
            swapRight->setLeft(node->getLeft()->getRight());
        }
        if (node->getLeft()->getLeft()->getLeft() != nullptr) {
            swapLeft->setLeft(node->getLeft()->getLeft()->getLeft());
            iterateHeight(swapLeft->getLeft(), 0);
        }
        if (node->getLeft()->getLeft()->getRight() != nullptr) {
            swapLeft->setRight(node->getLeft()->getLeft()->getRight());
            iterateHeight(swapLeft->getRight(), 0);
        }
        swap->setLeft(swapLeft);
        swap->setRight(swapRight);

        *node = *swap;

    }

    //Iterates the height of extra nodes after a rotation
    //Node is the node to be iterated, type is ++ or --
    void iterateHeight(AVLNode<T>* node, int type) {
        if (node != nullptr) {  //If node isn't null, recursively iterate again on it's children
            iterateHeight(node->getLeft(), type);
            iterateHeight(node->getRight(), type);
        }
        
        //If node isn't null, iterate by 1 if type = 1 and iterate by -1 if type = 0
        if (node != nullptr && type == 1) {
            node->setHeight(node->getHeight() + 1);
        }
        else if (node != nullptr && type == 0) {
            node->setHeight(node->getHeight() - 1);
        }
    }

    //Check AVL condition, return a key
    int checkAVL(AVLNode<T>* node, AVLNode<T>* insertedNode) {
        int key = 0;    //Init key as 0

        //Recursively check all relevant child nodes first to see if they need a rotation
        if (insertedNode->getData() < node->getData()) {
            key = checkAVL(node->getLeft(), insertedNode);
        }
        else if (insertedNode->getData() > node->getData()) {
            key = checkAVL(node->getRight(), insertedNode);
        }

        //If a rotation has been made, return out of function
        if (key != 0) {
            return key;
        }
        //If node is a leaf node with no children, return and go to parent node
        if (node->getLeft() == nullptr && node->getRight() == nullptr) {
            return 0;
        }
        //Other base cases where the node does not need rotation but is not a leaf node
        //Eg, node is a parent of 1 leaf node
        if (node->getLeft() != nullptr && node->getRight() == nullptr) {
            if (node->getLeft()->getRight() == nullptr && node->getLeft()->getLeft() == nullptr) {
                return 0;
            }
        }
        if (node->getRight() != nullptr && node->getLeft() == nullptr) {
            if (node->getRight()->getRight() == nullptr && node->getRight()->getLeft() == nullptr) {
                return 0;
            }
        }

        //Check AVL condition -> checks the height of either side, and rotates accordingly
        if (getHeight(node->getLeft()) > (getMinHeight(node->getRight()) + 1)) {
            rotateRight(node);
            return 1;
        }
        if (getHeight(node->getRight()) > (getMinHeight(node->getLeft()) + 1)) {
            rotateLeft(node);
            return 4;
        }

       
    }

    //Height function
    int getHeight(AVLNode<T>* node) {
        //If the node is a maximal leaf node, return it's height
        if (node == nullptr) {
            return 0;
        }
        else if (node->getLeft() == nullptr && node->getRight() == nullptr) {
            return node->getHeight();
        }
        else {
            //Otherwise, search for the next maximal height
            if (node->getLeft() != nullptr & node->getRight() != nullptr) {
                return max(getHeight(node->getLeft()), getHeight(node->getRight()));
            }
            //If either child is null, go the other way
            else if (node->getLeft() == nullptr) {
                return getHeight(node->getRight());
            }
            else if (node->getRight() == nullptr) {
                return getHeight(node->getLeft());
            }
            else {
                return 0;
            }
            
        }
    }

    int getMinHeight(AVLNode<T>* node) {
        //If the node is a minimal leaf node, return it's height
        //This is for the checkAVL condiditon, want to check if minimal left/right side + 1 is < maximal other side
        if (node == nullptr) {
            return 0;
        }
        else if (node->getLeft() == nullptr && node->getRight() == nullptr) {
            return node->getHeight();
        }
        else {
            //Otherwise, search for the next maximal height
            if (node->getLeft() != nullptr & node->getRight() != nullptr) {
                return min(getHeight(node->getLeft()), getHeight(node->getRight()));
            }
            //If either child is null, go the other way
            else if (node->getLeft() == nullptr) {
                return getHeight(node->getRight());
            }
            else if (node->getRight() == nullptr) {
                return getHeight(node->getLeft());
            }
            else {
                return 0;
            }
            
        }
    }
    //In order traversal
    void printInOrder(AVLNode<T>* node) {
        if (node->getLeft() != nullptr) {
            printInOrder(node->getLeft());
        }
        
        cout << "Node: " << node->getData() << endl;

        if (node->getRight() != nullptr) {
            printInOrder(node->getRight());
        }
    }

    //Prints each height of the tree
    void printAllHeights(AVLNode<T>* node) {

        for (int x = 0; x <= getHeight(rootNode); x++) {
            printAtHeight(node, x, 0);
            cout << endl;
        }
    }

    //Prints all the values on some height
	void printAtHeight(AVLNode<T>* node, int height, int current) {

		//If current level matches the target height
		if (current == height) {
			cout << node->getData() << " ";	//Print this value and return
			return;						
		}
		else {							
			int current2 = current+=1;  	//Iterate current
			if (node->getLeft() != nullptr) {	//If the node on the left of current node is not null
				printAtHeight(node->getLeft(), height, current2);	//Call printAtHeight recursively
			}
			if (node->getRight() != nullptr) {	//Do the same for the node on the right of the current node
				printAtHeight(node->getRight(), height, current2);
			}
		}

	}

    AVLNode<T>* getRoot() {
        return rootNode;
    }

    int validate() {
        int key = checkAVL(rootNode, rootNode);
        if (key == 0) { //If key = 0 (no rotations required)
            return 1;
        }
        else {
            return 0;
        }
    }
private:
    AVLNode<T>* rootNode;

};